export default {
  // devtools: { enabled: false },
  css: ['~/assets/sass/main.sass'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  mode: 'universal',
  modules: ['nuxt-gtag'],
  gtag: {
    id: 'G-VWWQP6DW64'
  }
}